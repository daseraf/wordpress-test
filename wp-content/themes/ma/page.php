<?php get_header(); ?>
<?php function debug($data){
    echo '<pre>' . print_r($data,1) . '</pre>';
}  ?>
<div class="container">
    <div class="slider-with-banner row">
    <div id="myCarousel" class="carousel slide column col-md-8" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
            <li data-target="#myCarousel" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="First slide">
                <div class="container">
                    <div class="carousel-caption text-left">
                        <h1>Example headline.</h1>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h1>Another example headline.</h1>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="third-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption text-right">
                        <h1>One more for good measure.</h1>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
        <div class="banner column col-md-4">
            <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/logoo-300x159.png" alt=""/>
            <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/logoo-300x159.png" alt=""/>
        </div>

    </div>
</div>

    <div class="container">
        <div class="featured-category">
            <div class="title-with-line">
                <span>Поиск по категориям</span>
            </div>
            <div class="featured-category-list">
                <div class="row">
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/car.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Кузовные детали</p></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/support.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Запчасти для ТО</p></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/optic.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Оптика</p></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/lamp.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Автолампы</p></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/disks.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Диски</p></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/accum.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Аккумуляторы</p></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/oil.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Моторные масла</p></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/while.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Шины</p></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-3 col-sm-4 col-md-2 col-lg-4">
                        <a href="#">
                            <div class="f-category">
                                <div class="f-category-media">
                                    <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/accsesuar.png" alt="">
                                </div>
                                <div class="f-category-caption"><p>Аксессуары</p></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid full-width-grey">
        <div class="container">
            <div class="row">
            <div class="featured-product-slider">
                <div class="featured-product-slider-owlbox column col-md-2">
                    <div class="featured-product-slider-title">
                        <span>Популярные товары</span>
                    </div>
                    <div class="owl-buttons">
                        <div class="owl-prev">
                            <span class="fa-links-slider-icon fa fa-angle-left fa-2x"></span>
                        </div>
                        <div class="owl-next">
                            <span class="fa-links-slider-icon fa fa-angle-right fa-2x"></span>
                        </div>
                    </div>
                </div>
                <div class="owl-wrapper olumn col-md-10">
                    <div class="owl-item col-xs-3 col-sm-4 col-md-2 col-lg-4 ">
                        <div class="product-item">
                            <div class="product-item-img">
                                <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/product.png" alt="">
                            </div>
                            <div class="product-item-details">
                                <span class="product-item-sku">1565789456</span>
                                <span class="product-item-name">Топливный фильтр для Mondeo</span>
                            </div>
                            <div class="product-item-info">
                                <div class="product-item-price">
                                    <span class="price-final_price">4444444 p.</span>
                                </div>
                                <div class="product-item-actions">
                                    <div class="actions-primary">
                                        <form data-role="add-tocart" action="" method="post">
                                            <button type="submit" title="В корзину" class="action add-to-cart">
                                                <span class="fa fa-shopping-cart"></span>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-item col-xs-3 col-sm-4 col-md-2 col-lg-4 ">
                        <div class="product-item">
                            <div class="product-item-img">
                                <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/product.png" alt="">
                            </div>
                            <div class="product-item-details">
                                <span class="product-item-sku">1565789456</span>
                                <span class="product-item-name">Топливный фильтр для Mondeo</span>
                            </div>
                            <div class="product-item-info">
                                <div class="product-item-price">
                                    <span class="price-final_price">4444444 p.</span>
                                </div>
                                <div class="product-item-actions">
                                    <div class="actions-primary">
                                        <form data-role="add-tocart" action="" method="post">
                                            <button type="submit" title="В корзину" class="action add-to-cart">
                                                <span class="fa fa-shopping-cart"></span>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-item col-xs-3 col-sm-4 col-md-2 col-lg-4 ">
                        <div class="product-item">
                            <div class="product-item-img">
                                <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/product.png" alt="">
                            </div>
                            <div class="product-item-details">
                                <span class="product-item-sku">1565789456</span>
                                <span class="product-item-name">Топливный фильтр для Mondeo</span>
                            </div>
                            <div class="product-item-info">
                                <div class="product-item-price">
                                    <span class="price-final_price">4444444 p.</span>
                                </div>
                                <div class="product-item-actions">
                                    <div class="actions-primary">
                                        <form data-role="add-tocart" action="" method="post">
                                            <button type="submit" title="В корзину" class="action add-to-cart">
                                                <span class="fa fa-shopping-cart"></span>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="title-with-line">
            <span>Поиск по марке автомобиля</span>
        </div>
    </div>
    <div class="container-fluid full-width-grey">
        <div class="container">
            <div class="featured-product-slider">
                <div class="column col-md-3">
                    <div class="featured-product-slider-title">
                        <span>Последние новости</span>
                    </div>
                </div>
                <div class="column col-md-9 row">

                    <?php
                    $args = array( 'posts_per_page' => 3 );
                    $lastposts = get_posts( $args );

                    foreach( $lastposts as $post ){
                        setup_postdata($post); // устанавливаем данные
                        ?>

                    <div class="column col-md-4">

                        <?php echo get_the_post_thumbnail( $post->{"id"}, 'thumbnail' ); ?>
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <?php the_date(); ?>
                        <?php the_content(); ?></div>

                        <?php
                    }
                    wp_reset_postdata(); // сброс
                    ?>

            </div>
        </div>
    </div>
    </div>
    <div class="container-fluid full-width-grey" style="background: #111; margin: -30px 0;">
    </div>

<div class="container-fluid">
    <div class="yandex-map">
    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af3026dffeaab894f37d986f5573aad4ea5398c16c3d9fd92093c048aa0c2d3e1&amp;width=100%25&amp;height=600&amp;lang=ru_RU&amp;scroll=true&amp;controls=[]"></script>
        <div class="yandex-map-caption">
            <h4>Контакты</h4>
            <p></p>
        </div>
        </div>
    </div>


<?php

$args = array('product_cat' => '%categiry_name%', 'post_type' => 'product');
$loop = new WP_Query( $args );
//debug($loop);
?>


<?php get_footer(); ?>