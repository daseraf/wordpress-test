<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <?php wp_head(); ?>
</head>

<body  <?php body_class(); ?>>
<header>
<div class="container-fluid">
        <div class="top-menu">
            <div class="container">
            <div class="menu-wrapper">
                    <div class="menu-header-container">
                        <div class="menu-header">
                            <?php $args = array(
                                'theme_location' => 'head_menu'
                            );
                            wp_nav_menu( $args ); ?>
                        </div>
                        <div class="user-account-link">
                            <div class="login">
                                <a href="">Войти</a>
                            </div>
                            <div class="sign-up">
                                <a href="">Регистрация</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
    <div class="header-primary">
    <div class="container">
        <div class="row">
            <div class="column grid12-4 header-logo">
                <img src="http://ma-emotion.box/wpclean/wp-content/uploads/2018/10/logoo-300x159.png" alt=""/>
            </div>
            <div class="column grid12-2">
                <div class="header-primary-address">
                    <span class="fa-links-slider-icon fa fa-map-marker fa-2x"></span>
                    <div class="address-top-string">
                        <p>Наш адрес</p>
                    </div>
                    <div class="address-middle-string"><p>Полюстровский пр-т, дом 59, корпус 1</p></div>
                </div>
            </div>
            <div class="column grid12-3">
                <div class="header-primary-address">
                    <span class="fa-links-slider-icon fa fa-phone fa-2x"></span>
                    <div class="address-top-string">
                        <p>Наш адрес</p>
                    </div>
                    <div class="address-middle-string"><p>Полюстровский пр-т, дом 59, корпус 1</p></div>
                </div>
            </div>
            <div class="column grid12-3">
                <div class="header-primary-cart">
                    <span class="fa-links-slider-icon fa fa-shopping-cart fa-2x"></span>
                    <div class="address-top-cart-string">
                        <p>В корзине</p>
                    </div>
                    <div class="address-middle-cart-string"><p>4 товара</p></div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="container-fluid">
        <div class="search-menu">
            <div class="container">
                <div class="search-form">
        <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>

</header>
